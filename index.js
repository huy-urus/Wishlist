// Main Express module
var express = require('express');

// Favicon module
var favicon = require('serve-favicon'); 

// Query string module
var querystring = require('querystring');

// Cookies parser module
var cookieParser = require('cookie-parser');

// Body parser module
var bodyParser = require('body-parser');

// Session module
var session = require('express-session'); 

// Path default module of Node
var path = require('path');

// Crypto default module of Node
var crypto = require('crypto');

// Request module to make REST
var request = require('request');

// Static app data json
var dataJson = require('./app.json');

// Underscore plugin
var _ = require('underscore');

// Init Express App
var app = express();

// Node postgres variabel
var pg = require('pg');
var pgSession = require('connect-pg-simple');
var connectionString = process.env.DATABASE_URL;
var conObj = {
    connectionString: connectionString,
   /* user: 'postgres',
    host: '127.0.0.1',
    database: 'mylocaldb',
    password: 'coolermaster1412', 
    port: 5432*/    
}

app.set('port', (process.env.PORT || 5000)); 
app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views'); 
app.set('view engine', 'ejs'); 

// setting favicon for app
app.use(favicon(__dirname + '/public/koala_icon.ico'));

// general settings for app
app.use(function(req, res, next) {
    res.removeHeader("X-Powered-By");
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Origin', req.headers.origin);    
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    store: new (pgSession(session))({
        conObject: conObj, 
    }),
    cookie: {
        path: '/apps'
    },
    secret: 'arenatheme-wishlist-app', 
    resave: false, 
    saveUninitialized: false,
}));

var pool = new pg.Pool(conObj);

/*-------------------------------- Index request handler ------------------------------------------*/
app.get('/apps', function(req, res) { 
    var shop = req.query.shop;
	// Check connection session is expired or not
    if(req.session['_token:' + shop] && (req.query.shop === req.session['last_shop'])) {
        res.render('pages/index', { 
            breadcrumb_title: dataJson.breadcrumb_title,
            api_key: dataJson.oauth.api_key,
            shop: req.session.shopname, 
        });
        
    } else {
        // If app installed on shopify store request will send with shop querystring
        if (req.query.shop) {
            res.redirect('/apps/shopify_auth?shopname=' + req.query.shop.replace('.myshopify.com',''));
        } else {
            res.redirect('/apps/install');    
        }
    }
});
/*-------------------------------------------------------------------------------------------------*/

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
app.get('/compares', function(req, res) {
    var shop = req.query.shop;
    //
    if(req.session['_token:' + shop] && (req.query.shop === req.session['last_shop'])) {
        res.render('pages/index', { 
            breadcrumb_title: dataJson.breadcrumb_title,
            api_key: dataJson.oauth.api_key,
            shop: req.session.shopname, 
        });
        
    } else {
        // If app installed on shopify store request will send with shop querystring
        if (req.query.shop) {
            res.redirect('/compares/shopify_auth?shopname=' + req.query.shop.replace('.myshopify.com',''));
        } else {
            res.redirect('/compares/install_compare');    
        }
    }
})

app.get('/compares/install_compare', function(req, res){
    res.render('pages/install_compare', {
        breadcrumb_title: dataJson.breadcrumb_title
    });
});
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


/*-------------------------------- Install redirect handler ---------------------------------------*/
app.get('/apps/install', function(req, res) {
	res.render('pages/install', {
        breadcrumb_title: dataJson.breadcrumb_title 
    });

});
/*-------------------------------------------------------------------------------------------------*/

/* ------------------------------- Init OAUTH process -------------------------------------------- */
app.get('/apps/shopify_auth', function(req, res) {
	/* This page just contain a script that redirect your browser
	to your shop admin page that contain some query string for request permission
	to install the app */
    if (req.query.shopname) {
        req.session.shopname = req.query.shopname;
        res.render('pages/embedded_app_redirect', {
            shop: req.query.shopname,
            api_key: dataJson.oauth.api_key, 				// api key that contained on app.json file
            scope: dataJson.oauth.scope,	   				// permission scope that contained on app.json file	
            redirect_uri: dataJson.oauth.redirect_uri		// redirect uri for next step in oauth process
        });
    }
})
/*-------------------------------------------------------------------------------------------------*/

/* ------------------------------- Get Access Token ----------------------------------------------- */
app.get('/apps/access_token', verifyRequest, getToken, installPageWL, installSnippetWL, function(req, res) {
    /* Finally Install script Tags in frontstore or excute main content of App here */
    var shop = req.session['last_shop'];
    var accessTok = req.session['_token:' + shop];

    // Check script Installed on front page
    var installedScript = false;
    request({
        url: 'https://' + shop + '/admin/script_tags.json', 
        method: "GET", 
        headers: {
            'X-Shopify-Access-Token': accessTok,
        },
    }, function(err, resp, body) {
        var scriptTags = JSON.parse(body).script_tags;

        if (scriptTags) {
            for (var i = 0; i < scriptTags.length; i++) {                            
                if (scriptTags[i].src.indexOf('bc-wishlist.js') != -1) {
                    installedScript = true;
                    break;
                }
            }
        }

        if (!installedScript) {
            var req_body = {
                script_tag: {
                    event: 'onload',
                    src: 'https:\/\/' + req.hostname + '\/script\/bc-wishlist.js'
                }
            }
            req_body = JSON.stringify(req_body);
            request({
                url: 'https://' + req.query.shop + '/admin/script_tags.json', 
                method: "POST",
                headers: {
                    'X-Shopify-Access-Token': accessTok,
                    'Content-type': 'application/json; charset=utf-8'
                },
                body: req_body
            },function(err, resp, body){
                if (body) {
                    console.log('Install Script Tag Successfully!');
                }
            });
        }
    });
    
    res.redirect('/apps?shop=' + req.query.shop);
    
});

/** INSTALL PAGE WISHLIST **/
function installPageWL(req,res,next) {
	var shop = req.session['last_shop'];
    var accessTok = req.session['_token:' + shop];
    console.log('installPageWL')

    /* Get List Page */
    request({
        url: 'https://' + shop + '/admin/pages.json?handle=wishlist-page', 
        method: "GET",
        headers: {
            'X-Shopify-Access-Token': accessTok,
            'Content-type': 'application/json; charset=utf-8'
        }    
    }, function(err, resp, body) {
        var listPageData = JSON.parse(body);
        if (listPageData.pages.length) {
            var pageId = listPageData.pages[0].id;    
        }
        
        
        /* If page existed just Update it*/
        if (listPageData.pages.length) {
            var update_body = {
                page: {
                    id: pageId,
                    template_suffix: 'wishlist'
                }
            }
            update_body = JSON.stringify(update_body);
            request({
                url: 'https://' + shop + '/admin/pages/' + pageId + '.json', 
                method: "PUT",
                headers: {
                    'X-Shopify-Access-Token': accessTok,
                    'Content-type': 'application/json; charset=utf-8'
                },
                body: update_body
            },function(err, resp, body){
                if (body) {
                    next();
                }
            });    
        } else { /* If page not exist create it */
            var create_body = {
                page: {
                    title: 'Wishlist Page',
                    handle: 'wishlist-page',
                    published: true,
                    template_suffix: 'wishlist'
                }
            }
            create_body = JSON.stringify(create_body);
            request({
                url: 'https://' + shop + '/admin/pages.json', 
                method: "POST",
                headers: {
                    'X-Shopify-Access-Token': accessTok,
                    'Content-type': 'application/json; charset=utf-8'
                },
                body: create_body
            },function(err, resp, body){
                if (body) {
                    next();
                }
            });
        }
    })	
}


/** INSTALL SNIPPET PAGE.WISHLIST TEST **/
function installSnippetWL(req,res,next) {

	var shop = req.session['last_shop'];
    var accessTok = req.session['_token:' + shop];
    /* Get Main theme First */
     request({
        url: 'https://' + shop + '/admin/themes.json?role=main', 
        method: "GET",
        headers: {
            'X-Shopify-Access-Token': accessTok,
            'Content-type': 'application/json; charset=utf-8'
        }    
    }, function(err, resp, body){
        var themeId = JSON.parse(body).themes[0].id;
        var fs = require('fs'); 
        fs.readFile(__dirname + '/public/page.wishlist.liquid', function(err, data) {
            var strData = data.toString();
            console.log(strData);
            var update_body = {
                "asset": {
                    "key": "templates\/page.wishlist.liquid",
                    "value": strData
                }
            }
            update_body = JSON.stringify(update_body);
            /* Get page.wishlist */
            request({
                url: 'https://' + shop + '/admin/themes/' + themeId + '/assets.json',
                method: 'PUT',
                headers: {
                    'X-Shopify-Access-Token': accessTok,
                    'Content-type': 'application/json; charset=utf-8'
                },
                body: update_body  
            }, function(err, resp, body) {
                next();
            });    
        });    
    });	
}


/*----------- THIS FUNCTION USE TO GET TOKEN FROM DB OR FROM NEW REQUEST TO SHOPIFY------------------------------*/
function getToken(req, res, next) {
    
    if (req.query.shop) {
        var shop = req.query.shop,
            renewToken = false,
            insertToken = false;
        // Get from DB first
        var selectQuery = {
            text: 'SELECT * FROM tbl_stores WHERE store_name = $1',
            values: [shop],
        };

        pool.connect(function(err, client, release) {
            if (err) {
                return console.error('Error acquiring client', err.stack);
            }
            client.query(selectQuery, function(err, data) { 

                // If there is no data return for store mark it have to insert info to db
                if (!data.rows[0]) {
                    insertToken = !insertToken;
                }

                // Because we have to wait function check valid 
                // token return to decide refresh new token or insert new token
                // We must run checkToken function with tempToken variable
                var tempToken = '';
                
                if (data.rows[0]) {
                    tempToken = data.rows[0].access_token;
                }

                checkToken(shop, tempToken, function(ret) {                 
                    if (ret) {  // Token is valid store it in session and go next middleware and have to return code here
                        req.session['_token:' + req.query.shop] = data.rows[0].access_token;
                        req.session['last_shop'] = req.query.shop; 
                        req.session.save();
                        release();
                        next();
                        return;   
                    } else {    // Token is invalid need refresh new one
                        renewToken = !renewToken;    
                    }  

                    // If we have to refresh new token or insert new token we have to
                    // POST to SHOPIFY to get access token
                    if (renewToken || insertToken) {
                        var params = { 
                            client_id: dataJson.oauth.api_key,
                            client_secret: dataJson.oauth.client_secret,
                            code: req.query.code
                        }
                        var req_body = querystring.stringify(params);
                        request({
                            url: 'https://' + shop + '/admin/oauth/access_token', 
                            method: "POST",
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                                'Content-Length': Buffer.byteLength(req_body)
                            },
                            body: req_body
                        },  
                        function(err,resp,body) {                        
                            body = JSON.parse(body);
                            req.session['_token:' + req.query.shop] = body.access_token;
                            req.session['last_shop'] = req.query.shop;
                            req.session.save();

                            var dbQuery = {};
                            if (renewToken && !insertToken) { // if refresh token just replace new valid token to the old one
                                dbQuery = {
                                    text: 'UPDATE tbl_stores SET access_token = $1 WHERE store_name = $2',
                                    values: [body.access_token, shop]    
                                };
                            }  

                            if (insertToken) {                  // Insert totally new token info not update
                                dbQuery = {
                                    text: 'INSERT INTO tbl_stores (store_name, access_token) VALUES ($1, $2)',
                                    values: [shop, body.access_token]
                                }
                            }
                            
                            client.query(dbQuery, function(err, data) {
                                if (err) {
                                    return console.error('Error executing query', err.stack);     
                                }
                                
                                if (data.rowCount) {
                                    release();
                                    next();
                                }
                            })
                        })   
                    }
                })                  
            })
        })
    }
}
/*-------------------------------------------------------------------------------------------------*/

/*----------- THIS FUNCTION USE TO GET TOKEN FROM DB FROM FRONTSTORE---------------------------------------------*/
function getTokenFromDb(req, res, next) {
    var shop = req.query.shop;
    // Get from DB first
    var selectQuery = {
        text: 'SELECT * FROM tbl_stores WHERE store_name = $1',
        values: [shop],
    };
    pool.connect(function(err, client, release) {
        if (err) {
            return console.error('Error acquiring client', err.stack);
        }
        client.query(selectQuery, function(err, data){
            release();
            if (data.rows[0]) {
                req.body.access_token = data.rows[0].access_token; 
                next();
            } else {
                return res.json({
                    'error': 'Can not get access token!!!'
                });
            }    
        })    
    })
}
/*-------------------------------------------------------------------------------------------------*/

/*----------- THIS FUNCTION USE TO VERIFY QUERY STRING THAT CLIENT STORE SEND TO REQUEST IN OAUTH ---------------*/
function verifyRequest(req, res, next) {
    
    var map = JSON.parse(JSON.stringify(req.query));
    delete map['signature'];
    delete map['hmac'];

    var message = querystring.stringify(map);
    var generated_hash = crypto.createHmac('sha256', dataJson.oauth.client_secret).update(message).digest('hex');

    if (generated_hash === req.query.hmac) {
        next();
    } else {
        return res.json(400); 
    }
}
/*-------------------------------------------------------------------------------------------------*/

/*----------- THIS FUNCTION USE TO VERIFY QUERY STRING THAT CLIENT STORE SEND TO REQUEST IN APP PROXY------------*/
function verifyRequestProxy(req, res, next) {

    // THIS CHECK FOR APP PROXY THAT WE NOT USE NOW
    /*var map = JSON.parse(JSON.stringify(req.query));
    var mapArr = []
    delete map['signature'];

    for (var key in map) {
        mapArr.push(key + '=' + map[key]);
    }

    var message = mapArr.sort().join('');
    var generated_hash = crypto.createHmac('sha256', dataJson.oauth.client_secret).update(message).digest('hex');

    if (generated_hash === req.query.signature) {
        next();
    } else {
        return res.json(req.query); 
    }*/

    /*var map = JSON.parse(JSON.stringify(req.headers));
    var origin = map['origin'];
    
    if (origin.indexOf('myshopify.com') != -1) {
        req.query.shop = origin.replace('https://', '');
        next();
    } else {
        return res.json({error: 'Request not from Shopify'}); 
    }*/

    var origin = req.query.shop;
    if (origin.indexOf('myshopify.com') != -1) {
        next();
    } else {
        return res.json({error: 'Request not from Shopify'}); 
    }
}
/*-------------------------------------------------------------------------------------------------*/

/*----------------------------------------- THIS FUNCTION USE CHECK VALID ACCESS TOKEN ------------*/
function checkToken(shop, tempToken, callback) {
    request({
        url: 'https://' + shop + '/admin/shop.json', 
        method: "GET",
        headers: {
            'X-Shopify-Access-Token': tempToken,
        },
    }, function(err, resp, body) {
        if (resp.statusCode == 401)
            return callback(false);
        else if (resp.statusCode == 200)  
            return callback(true);
    })    
}
/*-------------------------------------------------------------------------------------------------*/

/*-------------------------------- Function Handle wishlist ---------------------------------------*/
app.post('/wishlist-handle', verifyRequestProxy, getTokenFromDb, function(req, res) {
    var action = req.body.action,        
        customerId = req.body.customer,
        productHandle = req.body.productHandle,
        shop = req.query.shop;
    
    if (action == 'fullfill') {
        request({
            url: 'https://' + shop + '/admin/customers/' + customerId + '.json', 
            method: "GET",
            headers: {
                'X-Shopify-Access-Token': req.body.access_token 
            },
        }, function(err, resp, body) {
            console.log(body)
            body = JSON.parse(body);
            res.send({ 
                action: action, 
                status: "success!", 
                tags: body.customer.tags
            });
        });
    } else if (action == 'add') {
        updateWishlist(req, res, function(data) {
            res.send({action: action, status: "success!"});
        })
    } else {
        updateWishlist(req, res, function(data) {
            res.send({action: action, status: "success!"});
        })
    }
})

/*-------------------------- Function to add wishlist ----------------------------*/
function updateWishlist(req, res, callback) {
    console.log(req.body.productHandle)
    var req_body = JSON.stringify({
        customer: {
            id: req.body.customer,
            tags: req.body.productHandle,
        }    
    });
    request({
        url: 'https://' + req.query.shop + '/admin/customers/' + req.body.customer + '.json', 
        method: "PUT",
        headers: {
            'X-Shopify-Access-Token': req.body.access_token,
            'Content-type': 'application/json; charset=utf-8'
        },
        body: req_body
    }, function(err, resp, body) {
        //console.log(body)
        if (body) {
            var status = {
                action: req.body.action,
                status: JSON.stringify(body),     
            }
        }
        return callback(status);
    });
}

/*-------------------------------- App listen port config -----------------------------------------*/
app.listen(app.get('port'), function() {
  	console.log('Node app is running on port', app.get('port'));
});
/*-------------------------------------------------------------------------------------------------*/

(function(){
	var originalTags = '';
	var originalTagsArr = [];
	var fullfilLoaded = false;
	var appUrl = 'wishlist.arenathemes.com';
	var loadScript = function(url, callback) {
		var script = document.createElement("script")
	    script.type = "text/javascript";
	 
	    if (script.readyState){  //IE
	        script.onreadystatechange = function(){
	            if (script.readyState == "loaded" ||
	                    script.readyState == "complete"){
	                script.onreadystatechange = null;
	                callback();
	            }
	        };
	    } else {  //Others
	        script.onload = function(){
	            callback();
	        };
	    }
	 
	    script.src = url;
	    document.getElementsByTagName("head")[0].appendChild(script);
	};

	window.bcWishListJS = function($) {
		var originalTags = '';
		var originalTagsArr= [];
		var fullfilLoaded = false;
		var serverLink = 'https://wishlist.arenathemes.com/wishlist-handle?shop=' + Shopify.shop;
		var btnAddSelector = '.add-to-wishlist';
		var btnRemoveSelector = '.page-wishlist .remove-wishlist';
		var amountStatusSelector = '.show-wishlist .number';
		var amount = 0;
		var appUrl = 'wishlist.arenathemes.com';

		function uppAmountStatus(amount) {
			$(amountStatusSelector).html(amount);	
		}

		function addWishList(elem) {
			// Add loading state
			elem.addClass('pending'); 

			// Handle post data
			if (originalTags === '') {
				originalTags = 'wishlist--' + elem.data('handle-product');
			} else {
				originalTags += ', wishlist--' + elem.data('handle-product');
			}

			$.ajax({
				method: 'POST',
				url: serverLink,
				xhrFields: { withCredentials: true },
				data: {
					customer: meta.page.customerId,
					productHandle: originalTags,
					action: 'add',
				},
				error: function(xhr, stt, err){
					console.log(err);
				},
				success: function(data, stt, xhr) {
					uppAmountStatus(++amount);
					elem.removeClass('pending').addClass('added');
				}

			});
		}

		function removeWishList(elem, parent) {
			// Add loading state
			elem.addClass('pending');

			// Handle post data
			var replace = 'wishlist--' + elem.data('handle-product') + '[,\s]*';
        	var re = new RegExp(replace,"g");
        	originalTags = originalTags.replace(re, "");

			$.ajax({
				method: 'POST',
				url: serverLink,
				xhrFields: { withCredentials: true },
				data: {
					customer: meta.page.customerId,
					productHandle: originalTags,
					action: 'remove',
				},
				error: function(xhr, stt, err){
					console.log(err);
				},
				success: function(data, stt, xhr) {
					parent.remove();
					uppAmountStatus(--amount);
					if (!amount)
						$('.wishlist-table').addClass('hidden');
				}

			});
		}

		function addElemHandle() {
			// Add Handle
			$(btnAddSelector).off('click.wishlist').on('click.wishlist', function(evt) {
				var target = $(evt.currentTarget);
				
				/** Return Cases **/
				// If user not login 
				// If app has still not loaded list of user wishlist
				// If current product was in wishlist
				// If current product in middle of adding process
				if (!meta.page.customerId || !fullfilLoaded || target.hasClass('added') || target.hasClass('pending'))
					return;

				// Proceed add product to wishlist
				evt.preventDefault();
				addWishList(target);

			});

			// Remove Handle
			$(btnRemoveSelector).off('click.wishlist').on('click.wishlist', function(evt){
				var target = $(evt.currentTarget);
				var parent = target.parents('tr.wishlist-item');

				/** Return Cases **/
				// If current product in middle of removing process
				// If app has still not loaded list of user wishlist
				if (target.hasClass('pending') || !fullfilLoaded) {
					return;
				};

				// Proceed remove product to wishlist
				removeWishList(target, parent);
			});
		}

		return {
			
			init: function() {
				this.loadWishList();
				addElemHandle();
			},

			loadWishList: function() {
				/* Load User Wishlist Tags */
				if (meta.page.customerId) {
					$.ajax({
						method: 'POST',
						url: serverLink,
						xhrFields: { withCredentials: true },
						data: {
							customer: meta.page.customerId,
							action: 'fullfill'
						},
						error: function(xhr, stt, err){
							console.log(err);
						},
						success: function(data, stt, xhr) {
							originalTags = data.tags;

							if (!originalTags) {
								// Change state of fullfill
								fullfilLoaded = true;
								return;
							}
							
							originalTagsArr = originalTags.split(',');

							var tempTagsArr = []
							// remove other tags than  wishlist--
							for (var i = 0; i < originalTagsArr.length; i++) {
								if (originalTagsArr[i].indexOf('wishlist--') != -1) {
									tempTagsArr.push(originalTagsArr[i]);
								}
							}
							
							originalTagsArr = tempTagsArr;

							// Change state of fullfill
							fullfilLoaded = true;
							
							amount = originalTagsArr.length;
							uppAmountStatus(amount);
							// Update total number
							if (originalTagsArr.length) {
								
								$('.wishlist-table').removeClass('loading');
							} else {
								$('.wishlist-table').addClass('hidden');
							}
							
							// Update wishlist state buttons
							for (var j = 0; j < originalTagsArr.length; j++ ) {
								var tagItem = originalTagsArr[j].replace('wishlist--', '').trim();
								$(btnAddSelector + '[data-handle-product="' + tagItem + '"]').addClass('added');
							}
						}

					});
				}
			} 
		}
	}

	if ((typeof jQuery === 'undefined') || (parseFloat(jQuery.fn.jquery) < 1.7)) {
	  	loadScript('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', function(){
	    	jQuery191 = jQuery.noConflict(true);
	    	bcWishListJS(jQuery191).init();
	  	});
	} else {
	  	bcWishListJS(jQuery).init();
	}
})();